const mongoose = require('mongoose');
const ethPriceSchema = new mongoose.Schema(
  {
    inr: {
      type: Number,
    },
  },
  { timestamps: true }
);

module.exports = mongoose.model('EthereumPrice', ethPriceSchema);
