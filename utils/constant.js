//setting up all the variables at a place
module.exports = {
  ETH_PRICE_FETCH_INTERVAL: 600000,
  ETH_PRICE_URL: `https://api.coingecko.com/api/v3/simple/price?ids=ethereum&vs_currencies=inr`,
  USER_ADDRESS: process.env.USER_ADDRESS,
  ETHERSCAN_API_KEY: process.env.ETHERSCAN_API_KEY,
  PORT: process.env.PORT || 4000,
  MONGO_URI: process.env.MONGO_URI,
};
