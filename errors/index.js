const CustomAPIError = require('./custom-error');
const BadRequestError = require('./BadRequestError');

module.exports = {
  CustomAPIError,
  BadRequestError,
};
