const Transactions = require('../Models/TransactionModel');
const { StatusCodes } = require('http-status-codes');
const { BadRequestError } = require('../errors');
const axios = require('axios').default;
const { ETHERSCAN_API_KEY, USER_ADDRESS } = require('../utils/constant');
const API_URL = `https://api.etherscan.io/api?module=account&action=txlist&address=${USER_ADDRESS}&startblock=0&endblock=99999999&sort=asc&apikey=${ETHERSCAN_API_KEY}`;

const getUserAllTransactions = async (req, res) => {
  //calling the API to get the transactions of an user
  const object = await axios
    .get(API_URL)
    .then((result) => {
      return result.data;
    })
    .catch((err) => {
      console.error(err);
    });

  if (!object) {
    //handling error
    throw new BadRequestError(
      'Could not get the Transactions. Please try later'
    );
  }

  //destructuring the received response
  const { result } = object;
  const UserTransactions = {
    address: USER_ADDRESS,
    result,
  };

  //saving the transactions corresponding to an address
  const transactions = await Transactions.create(UserTransactions);
  return res.status(StatusCodes.OK).json(transactions);
};

module.exports = {
  getUserAllTransactions,
};
