const Transactions = require('../Models/TransactionModel');
const { StatusCodes } = require('http-status-codes');
const { BadRequestError } = require('../errors');
const ETH_PRICE_URL = `https://api.coingecko.com/api/v3/simple/price?ids=ethereum&vs_currencies=inr`;
const axios = require('axios').default;

//calling the API to get the latest price of ethereum
async function getEthPrice() {
  let data = await axios
    .get(ETH_PRICE_URL)
    .then((result) => {
      return result.data;
    })
    .catch((err) => {
      console.error(err);
    });
  return data;
}

const getUserBalance = async (req, res) => {
  const { id: address } = req.params;
  const EthPriceInRupees = await getEthPrice();
  //when a user with the same address makes a save all transactions request for the second time, since we are not updating the previous transactions but instead we are creating a new object that has all the previous and new transactions done by the user, so we sort by '-createdAt' such as to get the most recent of the transaction object
  const transactions = await Transactions.find({ address: address }).sort(
    '-createdAt'
  );

  if (transactions.length == 0) {
    throw new BadRequestError('no transactions for given user address');
  }

  const { result } = transactions[0];

  const balance = result.reduce((total, current) => {
    const { from, to, value } = current;
    if (address === from) {
      total -= value;
    }
    if (address === to) {
      total += value;
    }
    return total;
  }, 0);

  const BalanceData = {
    balance,
    EthPriceInRupees,
  };
  return res.status(StatusCodes.OK).json(BalanceData);
};

module.exports = {
  getUserBalance,
};
