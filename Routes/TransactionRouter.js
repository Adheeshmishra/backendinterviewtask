const express = require('express');
const TransactionRouter = express.Router();
const {
  getUserAllTransactions,
} = require('../Controller/TransactionController');

TransactionRouter.get('/transactions', getUserAllTransactions);
module.exports = TransactionRouter;
