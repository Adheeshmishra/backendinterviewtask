const express = require('express');
const BalanceRouter = express.Router();
const { getUserBalance } = require('../Controller/UserBalanceController');

BalanceRouter.get('/:id', getUserBalance);
module.exports = BalanceRouter;
