const EthereumPrice = require('../Models/EthereumPrice');
const axios = require('axios').default;
const {
  ETH_PRICE_URL,
  ETH_PRICE_FETCH_INTERVAL,
} = require('../utils/constant');
async function fetchEthPrice() {
  let data = await axios
    .get(ETH_PRICE_URL)
    .then((result) => {
      return result.data;
    })
    .catch((err) => {
      console.error(err);
    });
  return data;
}

const getPrice = setInterval(async () => {
  const EthPriceInRupees = await fetchEthPrice();
  const inr = EthPriceInRupees.ethereum;
  // console.log(inr);
  if (EthPriceInRupees) {
    await EthereumPrice.create(inr);
  }
}, ETH_PRICE_FETCH_INTERVAL);

module.export = getPrice;
