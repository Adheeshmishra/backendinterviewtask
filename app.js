require('dotenv').config();
const express = require('express');
const app = express();
// extra security packages
const helmet = require('helmet');
const cors = require('cors');
const xss = require('xss-clean');
const rateLimiter = require('express-rate-limit');

const getEthPrice = require('./Middlewares/EthereumPrice');
const TransactionRouter = require('./Routes/TransactionRouter');
const BalanceRouter = require('./Routes/UserBalanceRouter');
const connectDB = require('./db/connect');
const notFoundMiddleware = require('./Middlewares/notFound');
const { PORT, MONGO_URI } = require('./utils/constant');
//router

app.use('/', TransactionRouter);
app.use('/balance', BalanceRouter);

app.set('trust proxy', 1);
app.use(
  rateLimiter({
    windowMs: 15 * 60 * 1000, // 15 minutes
    max: 100, // limit each IP to 100 requests per windowMs
  })
);
app.use(express.json());
app.use(helmet());
app.use(cors());
app.use(xss());

app.get('/', (req, res) => {
  console.log('Koinx Assignment Test');
  res.send('Koinx Assignment Test');
});

app.use(notFoundMiddleware);
const start = async () => {
  try {
    //Connecting to the Database
    await connectDB(MONGO_URI);
    console.log('connected to the db');
    //Spinning the server
    app.listen(PORT, () =>
      console.log(`Server is listening on port ${PORT}...`)
    );
    //Calling the EthereumPrice middleware that helps us get the price of ethereum in every 10 minutes
    getEthPrice;
  } catch (error) {
    console.log(error);
  }
};

start();
